﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeFirstDBProject
{
    [Table ("products")]
    public class Product
    {
        [Column("product_id")]
        public int ProductId { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("category_id")]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

    }
}
