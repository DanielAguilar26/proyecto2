﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeFirstDBProject
{
    [Table("categories")]
    public class Category
    {
        private readonly ObservableListSource<Product> _products =
        new ObservableListSource<Product>();

        [Column("category_id")]
        public int CategoryId { get; set; }
        [Column("name")]
        public string Name { get; set; }

        public virtual ObservableListSource<Product> Products {
             get { return _products; } }
    }
}
